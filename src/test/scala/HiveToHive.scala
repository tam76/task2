package exercise.task2

import org.apache.spark.sql.{SparkSession, Dataset, Row}
import org.apache.spark.sql.functions._
import java.sql.{Timestamp, Date}

object HiveToHive {
  
  case class Number(FROM_PHONE_NUMBER: String, START_TIME: Timestamp, CALL_DURATION: Int, IMEI: String, LOCATION: String)

  case class Result(Phone: String, Date: java.sql.Date, NumberOfCall: Int, CallDuration: Int, WorkingHour: Int, MostIMEI: String, TopLocation: Array[String])

  val reduceNumber = (a: (String, Date, Int, Int, Int, List[String], List[String]), b: (String, Date, Int, Int, Int, List[String], List[String])) => {
    (
      a._1, 
      a._2,
      a._3 + b._3, 
      a._4 + b._4,
      a._5 + b._5,
      a._6 ::: b._6,
      a._7 ::: b._7
    )
  }
  
  val mapNumber = (number: Number) => {
    (
      number.FROM_PHONE_NUMBER,
      new Date(number.START_TIME.getTime()),
      1,
      number.CALL_DURATION,
      if (8 < number.START_TIME.getHours() && number.START_TIME.getHours() < 17) 1 else 0,
      List(number.IMEI),
      List(number.LOCATION)
    )
  }

  val processNumber = (fields: Iterator[Number]) => {
      fields
        .map(mapNumber(_))
        .reduce(reduceNumber(_,_))
  }

  def main(args: Array[String]) {

    val spark = SparkSession
      .builder
      .appName("Task 2 - Hive To Hive")
      .enableHiveSupport()
      .getOrCreate()
    import spark.implicits._

    val data = spark
      .sql("SELECT FROM_PHONE_NUMBER, START_TIME, CALL_DURATION, IMEI, LOCATION FROM call_history")
      .as[Number]

    val group = data
      .groupByKey(_.FROM_PHONE_NUMBER)
      .mapGroups { case(k, v) => processNumber(v) }
      .map(x => Result(
        x._1,
        x._2,
        x._3,
        x._4 ,
        x._5,
        x._6.groupBy(identity).mapValues(_.size).maxBy(_._2)._1 ,
        x._7.groupBy(identity).toSeq.sortWith(_._2.size > _._2.size).take(2).map(_._1).toArray))

    group.write
      .partitionBy("Date")
      .format("orc")
      .mode("ignore")
      .saveAsTable("daily_call_history")
  }
}

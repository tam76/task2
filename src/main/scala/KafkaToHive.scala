package exercise.task2

import org.apache.spark.sql.{SparkSession, Row}
import org.apache.spark.SparkConf
import org.apache.spark.streaming._
import org.apache.spark.streaming.StreamingContext._
import org.apache.spark.sql.types._
import java.util.{Properties, HashMap}
import java.sql.{Timestamp, Date}
import collection.JavaConverters._
import org.apache.spark.sql.functions._
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe

object KafkaToHive {

  object SparkSessionSingleton {

    @transient  private var instance: SparkSession = _

    def getInstance(sparkConf: SparkConf): SparkSession = {
      if (instance == null) {
        instance = SparkSession
          .builder
          .enableHiveSupport()
          .config(sparkConf)
          .getOrCreate()
      }
      instance
    }
  }

  val sparkConf = new SparkConf().setAppName("Task 2 - Kafka To Hive")
  val ssc = new StreamingContext(sparkConf, Seconds(10))
  val spark = SparkSessionSingleton.getInstance(sparkConf)
  import spark.implicits._
  
  case class Key(FROM_PHONE_NUMBER: String, START_TIME: Timestamp)
  case class Value(CALL_DURATION: Int, IMEI: String, LOCATION: String)
  case class Number(key: Key, value: Value)

  case class Result(Phone: String, Date: java.sql.Date, NumberOfCall: Int, CallDuration: Int, WorkingHour: Int, MostIMEI: String, TopLocation: Array[String])

  val reduceNumber = (a: (String, Date, Int, Int, Int, List[String], List[String]), b: (String, Date, Int, Int, Int, List[String], List[String])) => {
    (
      a._1,
      a._2,
      a._3 + b._3,
      a._4 + b._4,
      a._5 + b._5,
      a._6 ::: b._6,
      a._7 ::: b._7
    )
  }

  val mapNumber = (number: Number) => {
    (
      number.key.FROM_PHONE_NUMBER,
      new Date(number.key.START_TIME.getTime()),
      1,
      number.value.CALL_DURATION,
      if (8 < number.key.START_TIME.getHours() && number.key.START_TIME.getHours() < 17) 1 else 0,
      List(number.value.IMEI),
      List(number.value.LOCATION)
    )
  }

  val processNumber = (fields: Iterator[Number]) => {
    fields
      .map(mapNumber(_))
      .reduce(reduceNumber(_,_))
  }

  val schema = Schema.getSchema("call_history")
  val key = schema.apply(Set("FROM_PHONE_NUMBER", "TO_PHONE_NUMBER", "START_TIME"))
  val value = schema.apply(Set("CALL_DURATION", "IMEI", "LOCATION"))

  def main(args: Array[String]) {

    if (args.length < 1) {
      System.err.println("Usage: Task2 - Kafka To Hive <kafka bootstrap servers [node1.example.com:6667,node2.example.com:6667]>")
      System.exit(1)
    }

    // CONFIG FOR KAFKA CONSUMER //
    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> args(0),
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "tam76",
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )
    val topics = Array("call_history")

    // Process streaming here //
    val messages = KafkaUtils.createDirectStream[String, String](
      ssc,
      PreferConsistent,
      Subscribe[String, String](topics, kafkaParams)
    )

    val lines = messages
      .map(record => (
        record.key().toString(),
        record.value().toString))

    lines.foreachRDD { rdd =>
      if (!rdd.isEmpty) {
        val data = spark
          .createDataset(rdd)
          .select(
            from_json($"_1", key).alias("key"),
            from_json($"_2", value).alias("value"))
          .as[Number]

        val group = data.groupByKey(_.key.FROM_PHONE_NUMBER)
          .mapGroups { case(k, v) => processNumber(v) }
          .map(x => Result(
            x._1,
            x._2,
            x._3,
            x._4 ,
            x._5,
            x._6.groupBy(identity).mapValues(_.size).maxBy(_._2)._1 ,
            x._7.groupBy(identity).toSeq.sortWith(_._2.size > _._2.size).take(2).map(_._1).toArray))

        group.write
          .partitionBy("Date")
          .format("orc")
          .mode("ignore")
          .saveAsTable("daily_call_history")
      }
    }

    ssc.start()
    ssc.awaitTermination()
  }
}

package exercise.task2 
import org.apache.spark.sql.types._

object Schema {
  def getSchema(typeFile: String) : StructType = {
    if (typeFile == "call_history"){
      new StructType()
        .add("FROM_PHONE_NUMBER", StringType)
        .add("TO_PHONE_NUMBER", StringType)
        .add("START_TIME", TimestampType)
        .add("CALL_DURATION", IntegerType)
        .add("IMEI", StringType)
        .add("LOCATION", StringType)
    } else {
      new StructType()
    }

  }
  
}

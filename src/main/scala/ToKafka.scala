package exercise.task2

import org.apache.spark.sql.{SparkSession, Dataset, Row}
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import java.util.{Properties, HashMap}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}

object ToKafka {
  
  def main(args: Array[String]) {

    if (args.length < 7) {
      System.err.println("Usage: Task2 - To Kafka <format file> <have header[false||true]> <delimiter> <timestamp format [dd/MM/yyyy HH:mm:ss]> <type file [call_history]> <path file> <kafka bootstrap servers [node1.example.com:6667,node2.example.com:6667]>")
      System.exit(1)
    }

    val spark = SparkSession
      .builder
      .appName("Task 2 - To Kafka")
      .getOrCreate()
    import spark.implicits._

    // CONFIG FOR KAFKA PRODUCER //
    val props = new HashMap[String, Object]()
      props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, args(6))
      props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
        "org.apache.kafka.common.serialization.StringSerializer")
      props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
        "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String](props)
    
    def insertKafka(row: Row) {
      val message = new ProducerRecord[String, String](args(4), row.getString(0), row.getString(1))
      producer.send(message)
    }
                 
    // Get Data from file
    val data = spark
      .read
      .format(args(0)) // format of file
      .option("header", args(1))
      .option("delimiter", args(2))
      .option("inferSchema", "true")
      .option("timestampFormat", args(3))
      .schema(Schema.getSchema(args(4)))
      .load(args(5)) // path file in fourth param
      .select(
        to_json(struct($"FROM_PHONE_NUMBER", $"TO_PHONE_NUMBER", $"START_TIME")).alias("key"),
        to_json(struct($"CALL_DURATION", $"IMEI", $"LOCATION")).alias("value"))
    
    data.collect().foreach(insertKafka(_))
  }
}

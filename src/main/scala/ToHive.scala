package exercise.task2

import org.apache.spark.sql.{SparkSession, Dataset, Row}
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

object ToHive {
  
  def main(args: Array[String]) {

    if (args.length < 6) {
      System.err.println("Usage: Task2 - To Hive <format file> <have header[false||true]> <delimiter> <timestamp format [dd/MM/yyyy HH:mm:ss]> <type file [call_history]> <path file>")
      System.exit(1)
    }

    val spark = SparkSession
      .builder
      .appName("Task 2 - To Hive")
      .enableHiveSupport()
      .getOrCreate()
    import spark.implicits._

    // Get Data from file
    val data = spark
      .read
      .format(args(0)) // format of file
      .option("header", args(1))
      .option("delimiter", args(2))
      .option("escape", "/")
      .option("inferSchema", "true")
      .option("timestampFormat", args(3))
      .schema(Schema.getSchema(args(4)))
      .load(args(5)) // path file in fourth param
    
    data.write
      .format("orc")
      .mode("ignore")
      .saveAsTable(args(4))
  }
}

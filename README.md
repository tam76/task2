# TASK 2 #

Bigdata system design: Design a simple data pipeline for ETL and data aggregatio

### File in project included ###

* Folder data include file CSV test
* File code in src/main/scala/
* File src/main/scala/Schema.scala include Schema of file
* File jar conpiled in target/scala-2.11/*.jar
* Folder libs include file jar library

### My code do by ###

* Scala version 2.11.8
* Apache Spark 2.1.1
* Apache Hive 1.2.1
* Apache Kafka 0.10.1

### How do I compile code? ###

* Move to folder project
* run command line

sbt package

### Class in project include  ###

* class exercise.task2.ToHive: Send data from CDR file to Hive server
* class exercise.task2.HiveToHive: process data from Hive server to Hive server
* class exercise.task2.ToKafka: Send data from CDR file to Kafka
* class exercise.task2.KafkaToHive: process data from Kafka server to Hive server

### How do I run code ###

* Move to folder project
* run command line

bin/spark-submit --config value --jars path_lib target/scala-2.11/*.jar <args>

Example:

	class exercise.task2.ToHive:
	bin/spark-submit --master local[4] --executor-memory 8g --class exercise.task2.ToHive target/scala-2.11/*.jar csv false ";" 'dd/MM/yyyy HH:mm:ss' call_history file:///vagrant/test/data/no_header.csv
	
	class exercise.task2.ToKafka:
	bin/spark-submit --master local[4] --executor-memory 8g --class exercise.task2.ToKafka --jars path_lib target/scala-2.11/*.jar csv false ";" 'dd/MM/yyyy HH:mm:ss' call_history file:///vagrant/test/data/no_header.csv node1.example.com:6667,node2.example.com:6667
	
	class exercise.task2.HiveToHive:
	bin/spark-submit --master local[4] --executor-memory 8g --class exercise.task2.HiveToHive target/scala-2.11/*.jar
	
	class exercise.task2.KafkaToHive:
	bin/spark-submit --master local[4] --executor-memory 8g --class exercise.task2.KafkaToHive --jars path_lib target/scala-2.11/*.jar node1.example.com:6667,node2.example.com:6667

	create topic on Kafka:
	bin/kafka-topics.sh --create --zookeeper node1.example.com:2181,node2.example.com:2181 --replication-factor 2 --partitions 4 --topic call_history
	
# ARCHITECTURE DIAGRAM #

![Alt text](/img/architecture.png "Title")

# Why I choose Spark ? #

### OverView ###

Apache Spark is a lightning-fast cluster computing technology, designed for fast computation. It is based on Hadoop MapReduce and it extends the MapReduce model to efficiently use it for more types of computations, which includes interactive queries and stream processing. The main feature of Spark is its in-memory cluster computing that increases the processing speed of an application.

Spark is designed to cover a wide range of workloads such as batch applications, iterative algorithms, interactive queries and streaming. Apart from supporting all these workload in a respective system, it reduces the management burden of maintaining separate tools.

### Features of Apache Spark ###

Apache Spark has following features.

* Speed − Spark helps to run an application in Hadoop cluster, up to 100 times faster in memory, and 10 times faster when running on disk. This is possible by reducing number of read/write operations to disk. It stores the intermediate processing data in memory.

* Supports multiple languages − Spark provides built-in APIs in Java, Scala, or Python. Therefore, you can write applications in different languages. Spark comes up with 80 high-level operators for interactive querying.

* Advanced Analytics − Spark not only supports ‘Map’ and ‘reduce’. It also supports SQL queries, Streaming data, Machine learning (ML), and Graph algorithms.

# Why I choose Kafka ? #

### OverView ###

In Big Data, an enormous volume of data is used. Regarding data, we have two main challenges.The first challenge is how to collect large volume of data and the second challenge is to analyze the collected data. To overcome those challenges, you must need a messaging system.

Kafka is designed for distributed high throughput systems. Kafka tends to work very well as a replacement for a more traditional message broker. In comparison to other messaging systems, Kafka has better throughput, built-in partitioning, replication and inherent fault-tolerance, which makes it a good fit for large-scale message processing applications.

### Benifits ###

Following are a few benefits of Kafka −

* Reliability − Kafka is distributed, partitioned, replicated and fault tolerance.

* Scalability − Kafka messaging system scales easily without down time..

* Durability − Kafka uses Distributed commit log which means messages persists on disk as fast as possible, hence it is durable..

* Performance − Kafka has high throughput for both publishing and subscribing messages. It maintains stable performance even many TB of messages are stored.

### Use Cases ###

Kafka can be used in many Use Cases. Some of them are listed below −

* Metrics − Kafka is often used for operational monitoring data. This involves aggregating statistics from distributed applications to produce centralized feeds of operational data.

* Log Aggregation Solution − Kafka can be used across an organization to collect logs from multiple services and make them available in a standard format to multiple con-sumers.

* Stream Processing − Popular frameworks such as Storm and Spark Streaming read data from a topic, processes it, and write processed data to a new topic where it becomes available for users and applications. Kafka’s strong durability is also very useful in the context of stream processing.

# Why I choose Hive ? #

### Overview ###

Hive is a data warehouse infrastructure tool to process structured data in Hadoop. It resides on top of Hadoop to summarize Big Data, and makes querying and analyzing easy.

Initially Hive was developed by Facebook, later the Apache Software Foundation took it up and developed it further as an open source under the name Apache Hive. It is used by different companies. For example, Amazon uses it in Amazon Elastic MapReduce.

### Hive is not ###

* A relational database

* A design for OnLine Transaction Processing (OLTP)

* A language for real-time queries and row-level updates

### Features of Hive ###

* It stores schema in a database and processed data into HDFS.

* It is designed for OLAP.

* It provides SQL type language for querying called HiveQL or HQL.

* It is familiar, fast, scalable, and extensible.

### ORC File Format ##

The Optimized Row Columnar (ORC) file format provides a highly efficient way to store Hive data. It was designed to overcome limitations of the other Hive file formats. Using ORC files improves performance when Hive is reading, writing, and processing data.

Parquet vs ORC vs ORC with Snappy: https://stackoverflow.com/questions/32373460/parquet-vs-orc-vs-orc-with-snappy

# HARDWARE SERVER #

### 1 NameNode ###

* CPU: 8 core

* RAM: 64G

* 4 HDD 1T SATA 7.2k RPM   

* Include: NameNode (Hadoop)

### 1 SNameNode ###

* CPU: 12 core

* RAM: 128G

* 6 HDD 1T SATA 7.2k RPM   

* Include: SNameNode (Hadoop), Resource Manager (YARN), HiveServer

### 9 DataNode ###

Option 1: Collect data to HDFS (Hive with ORC) and Store daily basic to HDFS (Hive with ORC), process by Spark

* CPU: 12 core

* RAM: 128G

* 24 HDD 1T SATA 7.2k RPM   

* Include: DataNode (HDFS) store for 1 to 2 years, spark

Option 2: Collect data to Kafka and Store data daily basic to HDFS (Hive with ORC), process by Spark

* CPU: 20 core

* RAM: 256G

* 16 HDD 1T SATA 7.2k RPM   

* Include: Kafka broker collect data for 4 - 6 weeks, store data daily basic to HDFS (Hive with ORC) for 1 to 2 years, spark

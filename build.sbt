name := "Task2"

version := "1.0"

scalaVersion := "2.11.8"

sbtVersion := "0.13.12"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.1.1",
  "org.apache.spark" %% "spark-sql" % "2.1.1",
  "org.apache.spark" %% "spark-streaming" % "2.1.1",
  "org.apache.spark" %% "spark-hive" % "2.1.1",
  "org.apache.spark" %% "spark-streaming-kafka-0-10-assembly" % "2.1.1",
  "org.apache.kafka" % "kafka-clients" % "0.10.0.0")
